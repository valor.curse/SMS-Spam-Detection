import requests
import json

from urllib.parse import urlencode

IP = "192.168.49.2"
PORT = 32025
# IP = "localhost"
# PORT = 8080
URL = f"http://{IP}:{PORT}/predict"

if __name__ == "__main__":
    message = json.dumps({"sms": "lalalal"})
    
    highest_elapsed = 0
    lowest_elapsed = 10*1000
    while True:
        try:
            res = requests.post(URL, data=message)
            
            ms_elapsed = int(res.elapsed.microseconds/1000)
            if ms_elapsed > highest_elapsed:
                highest_elapsed = ms_elapsed
            if ms_elapsed < lowest_elapsed:
                lowest_elapsed = ms_elapsed

            print(" "*100, end='')
            print(f"\rCurrent: {ms_elapsed} Highest: {highest_elapsed} Lowest: {lowest_elapsed}", end='')
        except:
            continue