# Minikube

1. Install minikube
2. Start minikube with `minikube start --cpus 4 --memory "8192mb"`
3. Start all services in the `k8s` directory using `kubectl apply -f k8s`


# Netdata 

-- Install
1. Install Netdata using:
```bash
helm repo add netdata https://netdata.github.io/helmchart/
helm install netdata netdata/netdata
```

## Dashboard
1. Expose port of dashboard with `kubectl expose  deployment netdata-parent --type="NodePort" --port 19999`
2. Find your master IP with `kubectl get nodes --selector=node-role.kubernetes.io/master -o jsonpath='{$.items[*].status.addresses[?(@.type=="InternalIP")].address}'`
3. Find the port to your dashboard with `kubectl get service`
4. Go to your dashboard with using the values from step 2 & 3 as `http://<master ip>:<port>`

### Monitoring

To monitor the effects of the experiments on the pods, you can do the following:

1. On the left sidebar, select `minikube`
2. Then on the right sidebar, select the pod you'd like to monitor.

The monitoring is devided into multiple categories like cpu, memory and networking.
Select the one related to the experiment you are running.

# Chaos Mesh

# Setup
1. Install Chaos Mesh using `curl -sSL https://mirrors.chaos-mesh.org/v1.2.1/install.sh | bash`
2. Expose Chaos Mesh Dashboard with `kubectl port-forward -n chaos-testing svc/chaos-dashboard 2333:2333`
3. Run any experiment in `chaos-mesh-experiments` using `kubectl apply -f <name of experiment>.yaml`

# Run
1. Go to `chaos-mesh` folder
2. Enable experiment using `kubectl apply -f`, i.e. `kubectl apply -f cpu-burn.yaml`

# Chaostoolkit

# Setup 
1. Install python and create an virtual environment
2. Install Chaostoolkit using `pip install -U chaostoolkit chaostoolkit-kubernetes`

# Run
1. Go to `chaostoolkit` folder
2. Run experiment with `chaos run`, i.e. `chaos run cpu-burn.json`
