#!/bin/bash

if [ ! -d "k8s" ] 
then
    echo "Error: Directory k8s not found, please run from the project root.\n"
fi

kubectl apply -f k8s

helm repo add netdata https://netdata.github.io/helmchart/
helm install netdata netdata/netdata

kubectl expose  deployment netdata-parent --type="NodePort" --port 19999

curl -sSL https://mirrors.chaos-mesh.org/v1.2.1/install.sh | bash
kubectl port-forward -n chaos-testing svc/chaos-dashboard 2333:2333 &


echo "Your master IP is: "
echo $(kubectl get nodes --selector=node-role.kubernetes.io/master -o jsonpath='{$.items[*].status.addresses[?(@.type=="InternalIP")].address}')
